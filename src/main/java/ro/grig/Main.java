package ro.grig;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        System.out.println("=====================  Totally non-invasive data from sensors  =====================");

        List<MonitoredData> monitoredDataList;

        InputStream is = Main.class.getClassLoader().getResourceAsStream("activities.txt");
        assert is != null;
        BufferedReader br = new BufferedReader(new InputStreamReader(is));

        System.out.println("\nMonitored Data");
        monitoredDataList = br.lines()
                .map(line -> line.split("\t\t"))
                .map(parts -> new MonitoredData(parts[0].trim(), parts[1].trim(), parts[2].trim()))
                .toList();

        monitoredDataList.forEach(System.out::println);

        System.out.println("\nToileting...");
        Map<String, Integer> activityCounts = monitoredDataList.stream().collect(
            Collectors.groupingBy(
                (d) -> d.startTime().substring(0, 10),
                Collectors.summingInt(d -> d.activity().equals("Toileting") ? 1 : 0)
            )
        );

        activityCounts.forEach((activity, count) -> System.out.println(activity + ": " + count));

        System.out.println("\nActivities...");
        List<String> activities = monitoredDataList.stream()
                .map(Activity::new)
                .collect(Collectors.groupingBy(Activity::getActivity))
                .entrySet()
                .stream()
                .filter(entry -> {
                    int total = entry.getValue().size();
                    long shortDurationCount = entry.getValue()
                            .stream()
                            .filter(ac -> ac.getDuration() <= 5)
                            .count();

                    return shortDurationCount * 100 / 90 >= total;
                })
                .map(Map.Entry::getKey)
                .toList();

            activities.forEach(System.out::println);
    }

    private static class Activity {
        String activity;
        Long duration;

        Activity(MonitoredData origin) {
            activity = origin.activity();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

            LocalDateTime dateTime1 = LocalDateTime.parse(origin.startTime(), formatter);
            LocalDateTime dateTime2 = LocalDateTime.parse(origin.endTime(), formatter);

            duration = Duration.between(dateTime1, dateTime2).toMinutes();
        }

        public String getActivity() {
            return activity;
        }

        public Long getDuration() {
            return  duration;
        }

        @Override
        public String toString() {
            return activity + ": " + duration;
        }
    }
}