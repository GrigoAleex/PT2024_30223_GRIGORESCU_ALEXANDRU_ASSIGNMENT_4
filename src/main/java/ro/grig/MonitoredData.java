package ro.grig;

public record MonitoredData(String startTime, String endTime, String activity) { }
